package facci.anchundiacristina.conversionapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btnC, btnF;
    TextView txtResultado;
    EditText txtIngreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("MainActivity", "Anchundia Fernandez Cristina Elizabeth");
        btnC = (Button) findViewById(R.id.btnC);
        btnF = (Button) findViewById(R.id.btnF);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        txtIngreso = (EditText) findViewById(R.id.editIngreso);
        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double valor = Double.parseDouble(txtIngreso.getText().toString());
                Calcular(valor, "C");
            }
        });
        btnF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double valor = Double.parseDouble(txtIngreso.getText().toString());
                Calcular(valor, "F");
            }
        });
    }

    public void Calcular(double Valor, String Conversion) {
        Double Resultado = Valor;
        String Grados = "";
        if (Conversion == "C") {
            Resultado = ((Valor - 32) * 5 / 9);

        } else {
            if (Conversion == "F") {
                Resultado = ((Valor * 9) / 5) + 32;
            }
        }
        txtResultado.setText(Resultado + "\nGrados " + Grados);
    }
}
